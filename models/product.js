const { default: mongoose } = require('mongoose');
const monngoose = require('mongoose');
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        required: true
    },
    createdOn: {
        type: Date,
        required: new Date()
    },
    orders: [{
        orderId: {
            type: String,
            required: [true, "Order ID is required"]
        },
        required: [true, "Order is required"]
    }]
})

module.exports = mongoose.model("Products", productSchema);