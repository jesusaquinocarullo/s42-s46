const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/prodouctRoutes');
const app = expresss();
const port = process.env.PORT || 3000;

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI
app.use('/users', userRoutes);
app.use('/products', productRoutes);

// Mongoose Connection
mongoose.connect(`mongodb+srv://jcarullo:Doomsdays123@cluster0.srmn8hn.mongodb.net/s42-s46?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

app.listen(port, () => console.log(`API is now online at port: ${port}`));